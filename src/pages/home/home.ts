import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public countries: any[];

  constructor(public navCtrl: NavController, private http: Http) {

  }

  ngOnInit() {
    try {
      // var headers = new Headers();
      // headers.append('Access-Control-Allow-Origin' , '*');
      // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      // headers.append('Accept','application/json');
      // headers.append('content-type','application/json');
      // let options = new RequestOptionsArgs({ headers:headers});

      this.http.get('http://localhost:8000/api/country/')
      .map((res: Response) => {
        console.log('1');
        let body = res.json();
        console.log(body);
        return body || {};
      })
      .subscribe(countries => this.countries = countries.results);      
    } catch (error) {
      console.log('caiu no catch');
      console.log('Error: ' + error);
    }    

  }

}
